import sys
import requests
import re

url = "http://www.mangareader.net/i-am-a-hero"
ch = 1
pg = 1
imgRE = re.compile(r'<img id="img"[\w\W][^>]*>')
srcRE = re.compile(r'http:[\w\W][^>]*.jpg')
imageUrls = [[]]

def crawl():
    global pg
    global ch
    req = requests.get(url + "/" + str(ch) + "/" + str(pg))
    if req.status_code != 404:
        imgTag = imgRE.findall(req.text)[0]
        imageUrls[ch - 1].append( srcRE.findall(imgTag)[0] )
        pg += 1
        print imageUrls[ch-1][ len(imageUrls[ch-1]) - 1 ]
        crawl()
    else:
        print str(len(imageUrls[ch - 1])) + ' Pages in Ch ' + str(ch)
        ch += 1
        pg = 1
        imageUrls.append([])
        crawl()

crawl()